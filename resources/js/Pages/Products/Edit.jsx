import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';
import { Head,useForm,usePage,Link } from '@inertiajs/react';

export default function Dashboard(props) {
    const { product } = usePage().props;
    const { data, setData, put, errors } = useForm({
        name: product.name || "",
        desc: product.desc || "",
        price: product.price || "",
    });

    function handleSubmit(e) {
        e.preventDefault();
        put(route("product.update", product.id));
    }

    return (
        <AuthenticatedLayout auth={props.auth} user={props.auth.user} errors={props.errors} header={<h2 className="font-semibold text-xl text-gray-800 leading-tight">Edit Product</h2>}>
            <Head title="Product" />
            <div className="py-12">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
                    <div className="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                        <div className="p-6 bg-white border-b border-gray-200">
                            <div className="flex items-center justify-between mb-6">
                                <Link className="px-6 py-2 text-white bg-blue-500 rounded-md focus:outline-none" href={ route("product.index") }>Back</Link>
                            </div>
                            <form name="createForm" onSubmit={handleSubmit}>
                                <div className="flex flex-col">
                                    <div className="mb-4">
                                        <label className="">Name</label>
                                        <input type="text" className="w-full px-4 py-2" label="Name" name="name" value={data.name} onChange={(e) => setData("name", e.target.value) }/>
                                        <span className="text-red-600">
                                            {errors.name}
                                        </span>
                                    </div>
                                    <div className="mb-0">
                                        <label className="">Desc</label>
                                        <textarea type="text" className="w-full rounded" label="Desc" name="desc" errors={errors.desc} value={data.desc} onChange={(e) => setData("desc", e.target.value) }/>
                                        <span className="text-red-600">
                                        {errors.body}
                                        </span>
                                    </div>
                                     <div className="mb-4">
                                        <label className="">Price</label>
                                        <input type="text" className="w-full px-4 py-2" label="Price" name="price" value={data.price} onChange={(e) => setData("price", e.target.value) }/>
                                        <span className="text-red-600">
                                            {errors.price}
                                        </span>
                                    </div>
                                </div>
                                <div className="mt-4">
                                    <button type="submit" className="px-6 py-2 font-bold text-white bg-green-500 rounded">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </AuthenticatedLayout>
    );
}    